const fs = require('fs');
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

const { getPipelines } = require('./shared');

const toMinutes = x => x / (1000 * 60);

function analysePipelinesV1(pipelines) {
  const stats = {};

  pipelines.forEach(pipeline => {
    pipeline.jobs.forEach(job => {
      const { name, stage, duration } = job;

      const key = `${stage}:${name}`;

      if (!stats[key]) {
        stats[key] = [];
      }

      stats[key].push(duration);
    });
  });

  console.log(`Analyzed ${pipelines.length} pipelines`);
  console.log(`Average runtime of build jobs:`);
  Object.entries(stats).forEach(([name, values]) => {
    const average = values.reduce((sum, x) => sum + x, 0) / values.length;
    console.warn(name, `(${values.length} jobs)`, `${average} seconds`);
  });
}

function analysePipelinesV2(pipelines) {
  const stats = {};

  const max = {
    duration: 0,
    web_url: null,
  };

  pipelines.forEach(pipeline => {
    const pipelineStats = {
      startDate: pipeline.jobs[0].created_at,
      endDate: pipeline.jobs[0].finished_at,
      onMaster: pipeline.ref === 'master',
      day: pipeline.jobs[0].created_at.substr(0, 10),
    };

    pipeline.jobs.forEach(job => {
      const { name, stage, duration } = job;

      if (name !== 'review_stop') {
        if (pipelineStats.startDate > job.created_at) {
          pipelineStats.startDate = job.created_at;
        }

        if (pipelineStats.endDate < job.finished_at) {
          pipelineStats.endDate = job.finished_at;
        }
      }
    });

    const key = `${pipelineStats.onMaster ? 'master' : 'branch'}:${
      pipelineStats.day
    }`;

    const duration =
      new Date(pipelineStats.endDate) - new Date(pipelineStats.startDate);

    if (max.duration < duration) {
      max.duration = duration;
      max.web_url = pipeline.web_url;
    }

    if (!stats[key]) {
      stats[key] = [];
    }

    stats[key].push(toMinutes(duration));
  });

  console.log(`Analyzed ${pipelines.length} pipelines`);
  console.log(`Average runtime of build jobs:`);
  return Object.entries(stats)
    .map(([name, values]) => {
      const average = (
        values.reduce((sum, x) => sum + x, 0) / values.length
      ).toFixed(2);
      return `${name} (${values.length} jobs) ${average} minutes`;
    })
    .sort()
    .join('\n');
}

getPipelines()
  .then(pipelines => {
    analysePipelinesV1(pipelines);
    console.warn(analysePipelinesV2(pipelines));

    fs.writeFileSync(
      path.join(__dirname, 'results.json'),
      JSON.stringify(pipelines)
    );
  })
  .catch(e => {
    console.warn('An error happened');
    console.warn(e);
    process.exit(1);
  });
