# www gitlab jobs analysis

This project gives you the possibility to analyse project jobs

## Instructions

1. Run `yarn install` in the root folder of the project (one higher than this readme)
2. Copy `.env.example` to `.env` and fill in the data
3. Run `node ./index.js` in this folder
4. It may happen that you run into errors when loading a lot of data, as GitLab is rate limited.
   Either adjust the start date in the `.env` file or re-run after some time.
   Pipeline data is cached in a `cache/` folder, so subsequent reruns don't need to get the job
   data again.

## Example output

```
Average runtime of build jobs:

build:code_quality (417 jobs) 585.8699165755392 seconds
build:root_files_checker (500 jobs) 93.51828642999986 seconds
build:lint 0 2 (500 jobs) 174.6149091619998 seconds
build:lint 1 2 (501 jobs) 143.0821599041916 seconds
build:lint job_families (500 jobs) 147.68519570999993 seconds
build:rubocop (500 jobs) 167.35069397399985 seconds
build:spec 1 2 (500 jobs) 145.617484856 seconds
build:build_proxy_resource_master (90 jobs) 421.7158476666666 seconds
build:build_master 1/4 (93 jobs) 424.85739483870964 seconds
build:build_master 2/4 (92 jobs) 584.796468597826 seconds
build:build_master 3/4 (92 jobs) 501.8916418695652 seconds
build:build_master 4/4 (92 jobs) 570.8273561521739 seconds
build:dependency_scanning (500 jobs) 572.350250994 seconds
deploy:deploy_staging (500 jobs) 179.29763782400008 seconds
deploy:deploy (500 jobs) 175.58821688599977 seconds
deploy:apply_redirects_staging (11 jobs) 0 seconds
deploy:apply_redirects (11 jobs) 435.7048037272727 seconds
build:generate-handbook-changelog (3 jobs) 4184.985680666666 seconds
build:build_proxy_resource_base (2 jobs) 404.7266995 seconds
build:build_master 1/5 (408 jobs) 854.3723769779408 seconds
build:build_master 2/5 (408 jobs) 411.7128240906861 seconds
build:build_master 3/5 (408 jobs) 576.18799239951 seconds
build:build_master 4/5 (410 jobs) 491.83572272926847 seconds
build:build_master 5/5 (408 jobs) 562.6355076617651 seconds
build:codequality (83 jobs) 556.0585709277108 seconds
```

It will also write out the results to: `results.json`
