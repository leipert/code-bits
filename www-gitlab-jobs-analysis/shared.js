const path = require('path');
const superagent = require('superagent');
const fs = require('fs');

const projectID = encodeURIComponent(process.env.PROJECT);

const cachePath = id => path.join(__dirname, 'cache', `${id}.json`);

const cache = (filePath, pipeline) => {
  fs.writeFileSync(filePath, JSON.stringify(pipeline));
};

/*
 * Gets the jobs of a pipeline.
 */
async function getJobs(pipeline) {
  const { id } = pipeline;

  const filePath = cachePath(id);

  if (fs.existsSync(filePath)) {
    return JSON.parse(fs.readFileSync(filePath, 'utf8'));
  }

  const { body: jobs } = await superagent
    .get(`https://gitlab.com/api/v4/projects/${projectID}/pipelines/${id}/jobs`)
    .set('Private-Token', process.env.GITLAB_TOKEN);

  pipeline.jobs = jobs;

  cache(filePath, pipeline);

  return pipeline;
}

/*
 * Gets the pipelines of a project recursively (1000 max)
 */
async function getPipelines(page = 1) {
  console.warn(`Loading page ${page} of the pipelines.`);

  const { body: pipelinesRaw, headers } = await superagent
    .get(`https://gitlab.com/api/v4/projects/${projectID}/pipelines`)
    .set('Private-Token', process.env.GITLAB_TOKEN)
    .query({ status: 'success' })
    .query({ per_page: 100, page });

  const pipelines = await Promise.all(pipelinesRaw.map(getJobs));

  const lastDate = pipelines[pipelines.length - 1].created_at;

  console.warn(`Loaded until: ${lastDate}`);

  if (lastDate > process.env.START_DATE) {
    const nextPage = parseInt(headers['x-next-page'], 10);

    return [...pipelines, ...(await getPipelines(nextPage))];
  }

  return pipelines;
}

module.exports = {
  getJobs,
  projectID,
  getPipelines,
};
