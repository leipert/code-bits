const semver = require('semver');

module.exports = {
    processDocument(document) {
        try {
          const parsed = JSON.parse(document);
      
          if (!semver.validRange(parsed.range)) {
            throw new Error('RANGE_UNPARSEBLE: ' + parsed.range);
          }
      
          if (!semver.validRange(parsed.range)) {
            throw new Error('VERSION_UNPARSEBLE: ' + parsed.range);
          }
      
          const satisfies = semver.satisfies(parsed.version, parsed.range, {
            includePrerelease: true,
          });
      
          return { success: true, ...parsed, satisfies };
        } catch (e) {
          return {
            success: false,
            document: document,
            error: e.message,
          };
        }
      }      
}