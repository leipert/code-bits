#!/usr/bin/env node

let documents;

const bail = (error) => {
  console.log(JSON.stringify({success: false, error: error.message}))
  process.exit(1);
}

try {
  const fs = require('fs');

  if(!process.argv[2]){
    throw new Error('Please provide a file as input, e.g. fs.js <file.js>')
  }

  documents = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));

  if(!Array.isArray(documents)){
    throw new Error('File input is not a JSON array.')
  }

} catch(e){
  bail(e);
}

const {processDocument} = require('./shared');

console.log(JSON.stringify({success: true, documents: documents.map(processDocument)}))